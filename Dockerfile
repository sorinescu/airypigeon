# This container assumes the database volume 'airy-pigeon-dbdata' already exists
# and contains a properly initialized database.
#
# See 'README.md' for details.
#

FROM cybercode/alpine-ruby:2.3
RUN mkdir -p /usr/src/airy-pigeon
ADD . /usr/src/airy-pigeon
RUN mkdir -p /usr/src/airy-pigeon/log
COPY config/docker.config.yml /usr/src/airy-pigeon/config/config.yml
WORKDIR /usr/src/airy-pigeon
ENV AIRY_PIGEON_ENV production

# Make sure rubygems are up-to-date
RUN gem sources --remove https://rubygems.org/ && \
    gem update --system --source http://rubygems.org && \
    gem sources --add https://rubygems.org/

# To be able to connect to Rollbar over SSL, plus extra deps
RUN apk --update add ca-certificates libpq libxslt

# Install build tools, then [C-based] gems, then remove build tools to keep image small
RUN apk --update add --virtual build_deps \
    build-base ruby-dev libc-dev linux-headers \
    openssl-dev postgresql-dev libxml2-dev libxslt-dev && \
    bundle config build.nokogiri --use-system-libraries && \
    bundle install && \
    apk del build_deps

# Clean APK cache
RUN rm -rf /var/cache/apk/*

RUN bundle exec whenever --update-crontab

CMD crond && tail -F log/airy_cron.log