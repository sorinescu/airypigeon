# README #

This is an AirBnB scraper for locations in Brașov, Romania.
It's meant to be run at least once a day. It keeps a history of listing-related data, including daily prices and availability.

### How do I get set up? ###

* You need `ruby`, `bundle` and `PostgreSQL`
* You also need Docker and docker-compose
    * On OSX, you need [Docker for Mac](https://docs.docker.com/docker-for-mac/) (Docker Toolbox is not supported)
    * On Linux, follow the [Docker Engine and compose install procedure](https://docs.docker.com/compose/install/)
* Run `bundle install`
* To set up the database:
    * Create a Docker container from local DB: `docker volume create --name airy-pigeon-dbdata`
    * Run the database Docker image: `docker run --name airy-pigeon-db -p 5432:5432 -d -v airy-pigeon-dbdata:/var/lib/postgresql/data postgres:9.6`
    * Run `bundle exec rake db:setup`
    