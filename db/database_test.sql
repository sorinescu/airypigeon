-- noinspection SqlNoDataSourceInspectionForFile
-- Run with:
-- psql -d template1 -f database_test.sql

CREATE USER airy_pigeon WITH PASSWORD 'airy_pigeon';

DROP DATABASE airy_pigeon_test;
CREATE DATABASE airy_pigeon_test;
GRANT ALL PRIVILEGES ON DATABASE airy_pigeon_test to airy_pigeon;