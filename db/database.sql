-- noinspection SqlNoDataSourceInspectionForFile
-- Run with:
-- psql -d template1 -f database.sql

CREATE USER airy_pigeon WITH PASSWORD 'airy_pigeon';

DROP DATABASE airy_pigeon;
CREATE DATABASE airy_pigeon;
GRANT ALL PRIVILEGES ON DATABASE airy_pigeon to airy_pigeon;
