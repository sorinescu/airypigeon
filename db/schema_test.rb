require_relative 'schema'

puts 'Initializing test-specific DB tables'

DB.create_table :models_helpers_creation_time do
  primary_key :id
  Integer :key
  DateTime :created_at, null: false
end

DB.create_table :models_helpers_inherit_previous_values_owner do
  primary_key :id
end

DB.create_table :models_helpers_inherit_previous_values do
  primary_key :id
  foreign_key :owner_id, :models_helpers_inherit_previous_values_owner, null: false
  DateTime :created_at, null: false
  Integer :foo
  Integer :prev_foo
  Integer :bar
  Integer :prev_bar
  Integer :quux
end


DB.create_table :models_helpers_owner_columns_owner1 do
  primary_key :id
end

DB.create_table :models_helpers_owner_columns_owner2 do
  primary_key :id
end

DB.create_table :models_helpers_owner_columns do
  primary_key :id
  foreign_key :owner1_id, :models_helpers_owner_columns_owner1, null: false
  foreign_key :owner2_id, :models_helpers_owner_columns_owner2, null: false
  Integer :foo
end

DB.create_table :models_helpers_required_fields do
  primary_key :id
  DateTime :created_at
  Integer :foo
  Integer :prev_foo
  Integer :bar
  Integer :prev_bar
  Integer :quux
end

DB.create_table :models_listing_association do
  primary_key :id
  foreign_key :listing_id, :listings, null: false
  DateTime :created_at, null: false
  Integer :foo
  Integer :prev_foo
end
