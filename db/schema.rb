require 'sequel'
require 'pg'
require 'facets/string'
require_relative '../lib/initializers/database'

puts 'Initializing DB tables'

DB.create_table :hosts do
  Bignum :id, primary_key: true
  String :name, null: false
end

DB.create_table :discovered_listings do
  Bignum :id, primary_key: true
end

DB.create_table :listings do
  Bignum :id, primary_key: true
  foreign_key :host_id, :hosts, null: false
end

# Listing data that rarely changes
DB.create_table :listing_datas do
  primary_key :id
  foreign_key :listing_id, :listings, null: false
  DateTime :created_at, null: false
  TrueClass :new_listing, null: false
  TrueClass :prev_new_listing
  TrueClass :active, null: false
  TrueClass :prev_active
  String :name, null: false
  String :prev_name
  String :property_type, null: false
  String :prev_property_type
  String :room_type, null: false
  String :prev_room_type
  TrueClass :instant_book, null: false
  TrueClass :prev_instant_book
  TrueClass :super_host, null: false
  TrueClass :prev_super_host
  Integer :max_guests, null: false
  Integer :prev_max_guests
  Float :bedrooms, null: false
  Float :prev_bedrooms
  Float :bathrooms, null: false
  Float :prev_bathrooms
  Integer :king_beds, null: false
  Integer :prev_king_beds
  Integer :queen_beds, null: false
  Integer :prev_queen_beds
  Integer :single_beds, null: false
  Integer :prev_single_beds
  Integer :double_beds, null: false
  Integer :prev_double_beds
  Integer :sofa_beds, null: false
  Integer :prev_sofa_beds
  Integer :couches, null: false
  Integer :prev_couches
  Integer :cribs, null: false
  Integer :prev_cribs
  Integer :floor_mattresses, null: false
  Integer :prev_floor_mattresses
  Integer :toddler_beds, null: false
  Integer :prev_toddler_beds
  Integer :air_mattresses, null: false
  Integer :prev_air_mattresses
  Integer :bunk_beds, null: false
  Integer :prev_bunk_beds
  Integer :water_beds, null: false
  Integer :prev_water_beds
  Integer :hammocks, null: false
  Integer :prev_hammocks
  Integer :total_beds, null: false
  Integer :prev_total_beds
  TrueClass :business_travel, null: false
  TrueClass :prev_business_travel
  BigDecimal :extra_people_fee, null: false
  BigDecimal :prev_extra_people_fee
  Integer :guests_without_extra_fee, null: false
  Integer :prev_guests_without_extra_fee
  BigDecimal :cleaning_fee, null: false
  BigDecimal :prev_cleaning_fee
  BigDecimal :security_deposit, null: false
  BigDecimal :prev_security_deposit
  Float :weekly_discount_perc, null: false
  Float :prev_weekly_discount_perc
  Float :monthly_discount_perc, null: false
  Float :prev_monthly_discount_perc
  String :cancellation_policy, null: false
  String :prev_cancellation_policy
  column :amenities, 'text[]', null: false
  column :prev_amenities, 'text[]'
  TrueClass :allows_children, null: false
  TrueClass :prev_allows_children
  TrueClass :allows_infants, null: false
  TrueClass :prev_allows_infants
  TrueClass :allows_pets, null: false
  TrueClass :prev_allows_pets
  TrueClass :allows_smoking, null: false
  TrueClass :prev_allows_smoking
  TrueClass :allows_events, null: false
  TrueClass :prev_allows_events
end

# Listing data that changes often, but less than daily
DB.create_table :listing_ratings do
  primary_key :id
  foreign_key :listing_id, :listings, null: false
  DateTime :created_at, null: false
  Integer :review_count, null: false
  Integer :prev_review_count
  Integer :review_score, null: false
  Integer :prev_review_score
  Integer :review_score_accuracy, null: false
  Integer :prev_review_score_accuracy
  Integer :review_score_communication, null: false
  Integer :prev_review_score_communication
  Integer :review_score_cleanliness, null: false
  Integer :prev_review_score_cleanliness
  Integer :review_score_location, null: false
  Integer :prev_review_score_location
  Integer :review_score_checkin, null: false
  Integer :prev_review_score_checkin
  Integer :review_score_value, null: false
  Integer :prev_review_score_value
end

DB.create_table :listing_dates do
  primary_key :id
  foreign_key :listing_id, :listings, null: false
  DateTime :created_at, null: false
  Date :date, null: false
  BigDecimal :price
  BigDecimal :prev_price
  TrueClass :available, null: false
  TrueClass :prev_available
  String :price_type, null: false
  TrueClass :prev_price_type
end

DB.create_view :full_listings, <<-SQL.squish
  SELECT
    l.id,
    l.host_id,
    ld.new_listing,
    ld.active,
    ld.name,
    ld.property_type,
    ld.room_type,
    ld.instant_book,
    ld.super_host,
    ld.max_guests,
    ld.bedrooms,
    ld.bathrooms,
    ld.king_beds,
    ld.queen_beds,
    ld.single_beds,
    ld.double_beds,
    ld.sofa_beds,
    ld.couches,
    ld.cribs,
    ld.floor_mattresses,
    ld.toddler_beds,
    ld.air_mattresses,
    ld.bunk_beds,
    ld.water_beds,
    ld.hammocks,
    ld.total_beds,
    ld.business_travel,
    ld.extra_people_fee,
    ld.guests_without_extra_fee,
    ld.cleaning_fee,
    ld.security_deposit,
    ld.weekly_discount_perc,
    ld.monthly_discount_perc,
    ld.cancellation_policy,
    ld.amenities,
    ld.allows_children,
    ld.allows_infants,
    ld.allows_pets,
    ld.allows_smoking,
    ld.allows_events,
    lr.review_count,
    lr.review_score,
    lr.review_score_accuracy,
    lr.review_score_communication,
    lr.review_score_cleanliness,
    lr.review_score_location,
    lr.review_score_checkin,
    lr.review_score_value
  FROM listings l
  INNER JOIN (
    SELECT DISTINCT ON (listing_id) * FROM listing_datas ORDER BY listing_id, created_at DESC
  ) ld ON l.id = ld.listing_id
  INNER JOIN (
    SELECT DISTINCT ON (listing_id) * FROM listing_ratings ORDER BY listing_id, created_at DESC
  ) lr ON l.id = lr.listing_id
SQL
