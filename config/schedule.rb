# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

set :output, File.join(File.expand_path(File.dirname(__FILE__)), '../log/airy_cron.log')

set :environment_variable, 'AIRY_PIGEON_ENV'

every 10.hours do
	rake 'validate'
end

every 6.hours do
	rake 'discover'
end

every 1.day, :at => '11:30 pm' do
	rake 'update'
end
