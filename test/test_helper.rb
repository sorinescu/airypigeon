require_relative '../lib/initializers'
require 'minitest/autorun'
require 'minitest/hooks/default'
require 'minitest/reporters'

Minitest::Reporters.use!

class Minitest::HooksSpec
  def around
    Sequel::Model.db.transaction(:rollback=>:always, :auto_savepoint=>true) do
      super
    end
  end
end
