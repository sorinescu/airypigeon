require 'money'
require 'test_helper'
require 'monkey_patches/string'

describe Hash do
  describe '#require_keys!' do
    it 'succeeds if all required keys are present, regardless of order' do
      begin
        {foo: 1, bar: 2, baz: 3, quux: 4}.require_keys!(%i[foo bar])
        {foo: 1, bar: 2, baz: 3, quux: 4}.require_keys!(%i[bar foo])
        'OK'
      end.must_equal 'OK'
    end

    it 'fails if required keys is not an array' do
      proc { {foo: 1, bar: 2}.require_keys!(:foo) }.must_raise ArgumentError
    end

    it 'fails if at least one required key is missing, regardless of order' do
      proc { {foo: 1, bar: 2}.require_keys!(%i[foo baz]) }.must_raise ArgumentError
      proc { {foo: 1, bar: 2}.require_keys!(%i[baz foo]) }.must_raise ArgumentError
    end
  end
end
