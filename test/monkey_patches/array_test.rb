require 'test_helper'
require 'monkey_patches/array'

describe Array do
  describe '#each_with_next' do
    describe 'without block' do
      it 'returns empty array if length < 2' do
        [].each_with_next.must_be_empty
        [1].each_with_next.must_be_empty
      end

      it 'returns array of pairs [at(i), at(i+1)]' do
        [1, 2].each_with_next.must_equal [[1, 2]]
        [1, 2, 3, 4, 5].each_with_next.must_equal [[1, 2], [2, 3], [3, 4], [4, 5]]
      end
    end

    describe 'with block' do
      it 'passes each pair to block in turn' do
        expected = [[1, 2], [2, 3], [3, 4]]

        [1, 2, 3, 4].each_with_next do |pair|
          pair.must_equal expected.shift
        end
      end
    end
  end
end
