require 'money'
require 'test_helper'
require 'monkey_patches/string'

class TestMoneyExchange
  def self.get_rate(iso_from, iso_to)
    fail ArgumentError, "Unsupported destination currency #{iso_to}" if iso_to != 'EUR'
    case iso_from
      when 'EUR'
        1.0
      when 'USD'
        0.5
      when 'RON'
        0.25
      else
        0.1
    end
  end
end

describe String do
  before(:all) do
    Money.default_bank = Money::Bank::VariableExchange.new(TestMoneyExchange)
  end

  describe '#to_airbnb_euros' do
    it 'parses correct money string' do
      '$100.00'.to_airbnb_euros.must_equal 50.0
      '$10,000'.to_airbnb_euros.must_equal 5000.0
      'USD 10'.to_airbnb_euros.must_equal 5.00
      'USD10.4'.to_airbnb_euros.must_equal 5.2
      '€7'.to_airbnb_euros.must_equal 7.0

      'lei20'.to_airbnb_euros.must_equal 5.00
      'RON 40'.to_airbnb_euros.must_equal 10.00
    end

    it 'fails for incorrect money string' do
      proc { 'foo100.00'.to_airbnb_euros }.must_raise ArgumentError, "Unknown currency 'foo'"
      proc { '$100-00'.to_airbnb_euros }.must_raise ArgumentError
    end
  end
end
