require 'test_helper'
require 'page_parsers/helpers/latitude_longitude'

class TestPPHLL
  include PageParsers::Helpers::LatitudeLongitude
end

describe PageParsers::Helpers::LatitudeLongitude do
  describe '#bounds_from_coords' do
    it 'builds a Bounds from coordinates' do
      inst = TestPPHLL.new
      bounds = inst.bounds_from_coords(-1.0, 2.0, 3.0, 4.0)
      bounds.sw.lat.must_equal -1.0
      bounds.sw.lng.must_equal 2.0
      bounds.ne.lat.must_equal 3.0
      bounds.ne.lng.must_equal 4.0
    end
  end

  describe '#split_bounds' do
    it 'splits a bounds object into four pieces' do
      inst = TestPPHLL.new

      bounds = inst.bounds_from_coords(-30.0, -10.0, 20.0, 40.0)
      inst.split_bounds(bounds).size.must_equal 4
    end

    it 'generates pieces with coordinates inside bounds' do
      inst = TestPPHLL.new

      bounds = inst.bounds_from_coords(-30.0, -10.0, 20.0, 40.0)

      inst.split_bounds(bounds).each do |bound|
        bound.sw.lat.between?(-30.0, 20.0).must_equal true
        bound.sw.lng.between?(-10.0, 40.0).must_equal true
        bound.ne.lat.between?(-30.0, 20.0).must_equal true
        bound.ne.lng.between?(-10.0, 40.0).must_equal true
      end
    end

    it 'generates overlapping pieces' do
      inst = TestPPHLL.new

      bounds = inst.bounds_from_coords(-30.0, -10.0, 20.0, 40.0)

      res = inst.split_bounds(bounds)

      def dimension_intersects?(which, a, b)
        a.sw.public_send(which).between?(b.sw.public_send(which), b.ne.public_send(which)) ||
        a.ne.public_send(which).between?(b.sw.public_send(which), b.ne.public_send(which)) ||
        b.sw.public_send(which).between?(a.sw.public_send(which), a.ne.public_send(which)) ||
        b.ne.public_send(which).between?(a.sw.public_send(which), a.ne.public_send(which))
      end

      def bounds_intersect?(a, b)
        dimension_intersects?(:lat, a, b) && dimension_intersects?(:lng, a, b)
      end

      res.product(res).each do |bound_a, bound_b|
        bounds_intersect?(bound_a, bound_b).must_equal true
      end
    end
  end
end