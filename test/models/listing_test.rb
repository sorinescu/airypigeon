require 'facets/hash'
require 'test_helper'
require 'models/host'
require 'models/listing'

describe Models::Listing do
  before do
    Models::Host.create(id: 100, name: 'John Host')
    @fields = {
      host_id: 100,
      new_listing: true,
      active: true,
      name: 'Pimpy Crib',
      property_type: 'House',
      room_type: 'Bedroom',
      instant_book: false,
      super_host: false,
      max_guests: 6,
      bedrooms: 2,
      bathrooms: 1,
      king_beds: 1,
      queen_beds: 2,
      single_beds: 3,
      double_beds: 4,
      sofa_beds: 5,
      air_mattresses: 6,
      bunk_beds: 7,
      couches: 8,
      cribs: 9,
      floor_mattresses: 10,
      hammocks: 11,
      toddler_beds: 12,
      water_beds: 13,
      total_beds: 91,
      business_travel: true,
      extra_people_fee: 10,
      guests_without_extra_fee: 6,
      cleaning_fee: 20,
      security_deposit: 123.0,
      weekly_discount_perc: 5,
      monthly_discount_perc: 25,
      cancellation_policy: 'strict',
      amenities: %w[kitchen fire_extinguisher],
      allows_children: true,
      allows_infants: false,
      allows_pets: true,
      allows_smoking: false,
      allows_events: true,
      review_count: 66,
      review_score: 99,
      review_score_accuracy: 90,
      review_score_communication: 80,
      review_score_cleanliness: 70,
      review_score_location: 60,
      review_score_checkin: 50,
      review_score_value: 40
    }
  end

  describe '.create_or_update' do
    it 'requires all fields' do
      @fields.each_key do |field|
        fields = @fields.except field
        proc { Models::Listing.create_or_update(1, fields) }.must_raise ArgumentError
      end
    end

    it 'creates or updates listing' do
      model = Models::Listing.create_or_update(1, @fields)
      model.id.must_equal 1
      model.host_id.must_equal 100
    end

    it 'creates or updates ListingData' do
      model = Models::Listing.create_or_update(1, @fields)
      model.listing_datas.count.must_equal 1

      data = model.listing_datas.first

      data.listing_id.must_equal 1
      data.values.except(:id, :listing_id, :created_at).each do |column, value|
        @fields[column].must_equal value
      end
    end

    it 'creates or updates ListingRating' do
      model = Models::Listing.create_or_update(1, @fields)
      model.listing_ratings.count.must_equal 1

      ratings = model.listing_ratings.first

      ratings.listing_id.must_equal 1
      ratings.values.except(:id, :listing_id, :created_at).each do |column, value|
        @fields[column].must_equal value
      end
    end
  end
end
