require 'test_helper'
require 'models/host'
require 'models/listing'
require 'models/listing_association'

class TestModelMLA < Sequel::Model(:models_listing_association)
  include Models::ListingAssociation
end

describe Models::ListingAssociation do
  describe '.create_if_changed' do
    before do
      Models::Host.create(id: 100, name: 'John Host')
      @listing = Models::Listing.create(id: 1000, host_id: 100)
    end

    it 'creates an entry if none exists' do
      model = TestModelMLA.create_if_changed(listing_id: @listing.id, foo: 1)

      model.created_at.must_be_close_to Time.now, 0.01
      model.foo.must_equal 1
      model.prev_foo.must_be_nil
    end

    it 'creates new entry and inherits previous values if fields have changed' do
      TestModelMLA.create_if_changed(listing_id: @listing.id, created_at: Time.now - 20, foo: 10)
      model = TestModelMLA.create_if_changed(listing_id: @listing.id, foo: 20)

      model.created_at.must_be_close_to Time.now, 0.01
      model.foo.must_equal 20
      model.prev_foo.must_equal 10
    end

    it "doesn't create entry if fields haven't changed" do
      TestModelMLA.create_if_changed(listing_id: @listing.id, created_at: Time.now - 20, foo: 10)
      model = TestModelMLA.create_if_changed(listing_id: @listing.id, foo: 10)

      model.created_at.must_be_close_to Time.now - 20, 0.01
      model.foo.must_equal 10
      model.prev_foo.must_be_nil
    end

    it 'inherits previous values for the right listing' do
      other_listing = Models::Listing.create(id: 1001, host_id: 100)

      TestModelMLA.create_if_changed(listing_id: @listing.id, created_at: Time.now - 20, foo: 10)
      TestModelMLA.create_if_changed(listing_id: other_listing.id, created_at: Time.now - 10, foo: 11)
      model = TestModelMLA.create_if_changed(listing_id: @listing.id, foo: 20)

      model.prev_foo.must_equal 10
    end
  end
end
