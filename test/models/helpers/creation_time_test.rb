require 'test_helper'
require 'models/helpers/creation_time'

class TestModelMHCT < Sequel::Model(:models_helpers_creation_time)
  include Models::Helpers::CreationTime
end

describe Models::Helpers::CreationTime do
  describe 'during creation' do
    it 'automatically adds created_at' do
      model = TestModelMHCT.create
      model.created_at.must_be_close_to Time.now, 0.01
    end
  end

  describe '.latest_for' do
    it 'returns nil if no previous instance exists' do
      TestModelMHCT.latest_for(key: 1).must_be_nil
    end

    it 'filters by single field' do
      model_id1 = TestModelMHCT.create(key: 1)
      model_key1 = TestModelMHCT.create(key: 1)
      model_key2 = TestModelMHCT.create(key: 2)

      TestModelMHCT.latest_for(key: 1).must_equal model_key1
      TestModelMHCT.latest_for(key: 2).must_equal model_key2
      TestModelMHCT.latest_for(id: model_id1.id).must_equal model_id1
      TestModelMHCT.latest_for(key: 3).must_be_nil
    end

    it 'filters by multiple fields' do
      model = TestModelMHCT.create(key: 1)

      TestModelMHCT.latest_for(id: model.id, key: 1).must_equal model
      TestModelMHCT.latest_for(id: model.id, key: 2).must_be_nil
    end

    it 'returns the instance with the greatest created_at' do
      TestModelMHCT.create(key: 1)
      model = TestModelMHCT.create(key: 1, created_at: Time.now + 10)
      TestModelMHCT.create(key: 1)

      TestModelMHCT.latest_for(key: 1).must_equal model
    end
  end
end
