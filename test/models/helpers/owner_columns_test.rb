require 'test_helper'
require 'models/helpers/owner_columns'

class TestModelOwner1MHOC < Sequel::Model(:models_helpers_owner_columns_owner1)
end

class TestModelOwner2MHOC < Sequel::Model(:models_helpers_owner_columns_owner2)
end

class TestModelMHOC < Sequel::Model(:models_helpers_owner_columns)
  include Models::Helpers::OwnerColumns

  many_to_one :models_helpers_owner_columns_owner1, key: :owner1_id
  many_to_one :models_helpers_owner_columns_owner2, key: :owner2_id
end

describe Models::Helpers::OwnerColumns do
  before do
    @owner1 = TestModelOwner1MHOC.create
    @owner2 = TestModelOwner2MHOC.create
    @model = TestModelMHOC.create(owner1_id: @owner1.id, owner2_id: @owner2.id)
  end

  describe '#owner_columns' do
    it 'gets all many-to-one relations as owner columns' do
      @model.owner_columns.must_equal %i[owner1_id owner2_id]
    end
  end

  describe '#owner_columns_values' do
    it 'gets the values of all owner columns of current instance' do
      @model.owner_columns_values.must_equal owner1_id: @owner1.id, owner2_id: @owner2.id
    end
  end

  describe '#owner_columns_from_fields' do
    it 'gets the values of all owner columns from fields' do
      @model.owner_columns_from_fields(foo: 1, owner1_id: 2, owner2_id: 3).must_equal owner1_id: 2, owner2_id: 3
    end

    it "fails if fields don't contain all owner columns as keys" do
      proc { @model.owner_columns_from_fields(foo: 1, owner1_id: 2) }.must_raise ArgumentError
    end
  end
end
