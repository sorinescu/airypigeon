require 'test_helper'
require 'models/helpers/inherit_previous_values'

class TestModelOwnerMHIPV < Sequel::Model(:models_helpers_inherit_previous_values_owner)
end

class TestModelMHIPV < Sequel::Model(:models_helpers_inherit_previous_values)
  include Models::Helpers::InheritPreviousValues

  many_to_one :models_helpers_inherit_previous_values_owner, key: :owner_id
end

describe Models::Helpers::InheritPreviousValues do
  it "inherits previous values for all fields that have an associated 'prev_*' field" do
    owner = TestModelOwnerMHIPV.create
    TestModelMHIPV.create(owner_id: owner.id, foo: 1, bar: 2, quux: 3, created_at: Time.now - 2)
    model = TestModelMHIPV.create(owner_id: owner.id, foo: 10, bar: 20, quux: 30, created_at: Time.now)

    model.prev_foo.must_equal 1
    model.prev_bar.must_equal 2
  end

  it 'inherits the value from the previous instance with the greatest created_at' do
    owner = TestModelOwnerMHIPV.create
    TestModelMHIPV.create(owner_id: owner.id, foo: 1, created_at: Time.now - 2)
    TestModelMHIPV.create(owner_id: owner.id, foo: 100, created_at: Time.now - 3)
    model = TestModelMHIPV.create(owner_id: owner.id, foo: 10, created_at: Time.now)

    model.prev_foo.must_equal 1
  end

  it 'inherits the value for the proper owner' do
    owner1 = TestModelOwnerMHIPV.create
    owner2 = TestModelOwnerMHIPV.create

    TestModelMHIPV.create(owner_id: owner2.id, foo: 1, created_at: Time.now - 2)

    TestModelMHIPV.create(owner_id: owner1.id, foo: 100, created_at: Time.now - 3)
    model = TestModelMHIPV.create(owner_id: owner1.id, foo: 10, created_at: Time.now)

    model.prev_foo.must_equal 100
  end
end
