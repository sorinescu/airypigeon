require 'test_helper'
require 'models/helpers/required_fields'

class TestModelMHRF < Sequel::Model(:models_helpers_required_fields)
  include Models::Helpers::RequiredFields
end

describe Models::Helpers::RequiredFields do
  describe '.REQUIRED_FIELDS' do
    it 'equals all fields except prev_*, id, created_at' do
      TestModelMHRF::REQUIRED_FIELDS.must_equal %i[foo bar quux]
    end
  end

  describe '#fields_changed?' do
    before do
      @model = TestModelMHRF.create(created_at: Time.now, foo: 1, bar: 2, quux: 3)
    end

    it "fails if fields don't contain all required fields as keys" do
      proc { @model.fields_changed?(foo: 1, bar: 2) }.must_raise ArgumentError
    end

    it 'returns true if any of the required fields in argument are different from the ones in the model' do
      @model.fields_changed?(foo: 1, bar: 2, quux: 4).must_equal true
      @model.fields_changed?(foo: 1, bar: 4, quux: 3).must_equal true
      @model.fields_changed?(foo: 4, bar: 2, quux: 3).must_equal true
    end

    it 'returns false if none of the required fields in argument are different from the ones in the model' do
      @model.fields_changed?(foo: 1, bar: 2, quux: 3).must_equal false
    end

    it 'ignores non-required fields' do
      @model.fields_changed?(created_at: Time.now + 10, foo: 1, bar: 2, quux: 3).must_equal false
    end
  end
end
