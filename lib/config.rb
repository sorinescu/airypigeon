require 'yaml'
require 'facets/hash'

class Config
  def self.method_missing(name)
    name = name.to_s

    # production?, test?, development?
    return (ENV['AIRY_PIGEON_ENV'] == name.chop) if name.end_with?('?')

    value = hash[name]
    fail NoMethodError, name if value.nil?

    if value.is_a? Hash
      ConfigHolder.new(value)
    else
      value
    end
  end

  private

  def self.hash
    @hash ||= begin
      env = ENV['AIRY_PIGEON_ENV']
      fail RuntimeError, 'AIRY_PIGEON_ENV is not set' if env.nil?

      config = YAML.load_file('config/config.yml')
      config['common'].deep_merge config[env]
    end
  end

  class ConfigHolder
    def initialize(hash)
      @hash = hash
    end

    def method_missing(name)
      value = @hash[name.to_s]
      fail NoMethodError, name if value.nil?

      if value.is_a? Hash
        ConfigHolder.new(value)
      else
        value
      end
    end
  end
end
