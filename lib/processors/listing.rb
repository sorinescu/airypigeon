require 'facets/hash'
require_relative '../models/host'
require_relative '../models/listing'

module Processors
  class Listing
    def self.mark_inactive(listing_id)
      listing = Models::Listing[listing_id]
      listing.try(:update, active: false)
    end

    # @param parsed_listing PageParsers::Listing
    def initialize(parsed_listing)
      ids = Set.new(Models::Listing.all_ids)


      @attributes = {
        new_listing: !ids.include?(parsed_listing.id),
        active: true
      }.merge(parsed_listing.attributes_hash)
      @host_attributes = @attributes.remove!(:host_name).rekey(host_name: :name)

      @listing_id = @attributes.delete(:id)
    end

    def save
      Models::Host.create_or_update(@attributes[:host_id], @host_attributes)
      Models::Listing.create_or_update(@listing_id, @attributes)
    end
  end
end
