require 'geokit'

module PageParsers
  module Helpers
    module LatitudeLongitude

      EARTH_RADIUS = 6371000  # meters

      # @param bounds Geokit::Bounds
      # @return Array[Geokit::Bounds] [SW, SE, NE, NW]
      def split_bounds(bounds)
        center = bounds.center

        lat_ranges = overlapping_ranges(bounds.sw.lat, bounds.ne.lat, center.lat)
        lng_ranges = overlapping_ranges(bounds.sw.lng, bounds.ne.lng, center.lng)

        lat_ranges.product(lng_ranges).map do |lat_range, lng_range|
          bounds_from_coords(lat_range.first, lng_range.first, lat_range.last, lng_range.last)
        end
      end

      # @return Array[Float] two overlapping ranges
      def overlapping_ranges(min, max, center)
        [[min, center + (max - center) * 0.25],
         [center - (center - min) * 0.25, max]]
      end

      # @return Geokit::Bounds
      def bounds_from_coords(sw_lat, sw_lng, ne_lat, ne_lng)
        sw = Geokit::LatLng.new(sw_lat, sw_lng)
        ne = Geokit::LatLng.new(ne_lat, ne_lng)
        Geokit::Bounds.new(sw, ne)
      end

      # Adapted from http://stackoverflow.com/questions/6048975/google-maps-v3-how-to-calculate-the-zoom-level-for-a-given-bounds
      def zoom_for_bounds(bounds, map_pixels_w, map_pixels_h)
        world_h, world_w = 256, 256
        zoom_max = 21

        def lat_radians(lat)
          sin = Math.sin(lat * Math::PI / 180.0)
          rad_x2 = Math.log((1 + sin) / (1 - sin)) / 2
          [[rad_x2, Math::PI].min, -Math::PI].max / 2
        end

        def zoom(map_px, world_px, fraction)
          (Math.log(map_px / world_px / fraction) / Math.log(2)).floor
        end

        ne = bounds.ne
        sw = bounds.sw

        lat_fraction = (lat_radians(ne.lat) - lat_radians(sw.lat)) / Math::PI;

        lng_diff = ne.lng - sw.lng
        lng_fraction = ((lng_diff < 0) ? (lng_diff + 360) : lng_diff) / 360

        lat_zoom = zoom(map_pixels_h, world_h, lat_fraction)
        lng_zoom = zoom(map_pixels_w, world_w, lng_fraction)

        [lat_zoom, lng_zoom, zoom_max].min
      end
    end
  end
end
