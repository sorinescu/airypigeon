require 'geokit'
require_relative 'paged_html_parser'

module PageParsers
  # Should not be used by itself, use CityListings instead
  class ListingsByGeoBounds < PagedHTMLParser
    attr_reader :listing_urls

    def initialize(city_province_country, bounds, zoom)
      validate_city_province_country!(city_province_country)
      fail ArgumentError unless bounds.is_a? Geokit::Bounds

      @city_province_country = city_province_country
      @bounds = bounds
      @zoom = zoom

      url = "https://www.airbnb.com#{relative_url(1)}"
      # puts url

      super(url)

      @listing_ids = []
      parse_each do |page|
        page.css('div.listing').map do |listing|
          @listing_ids << listing_url(page, listing)
        end
      end
    end

    def listing_url(page, listing)
      url = listing.css('.listing-name').css('a').first.attr('href')
      page.uri.merge(url).to_s
    end

    private

    # @param link Mechanize::Page::Link
    def click_link(link)
      page_no = link.href[/page=(\d+)/, 1].to_i
      new_link = { 'href' => relative_url(page_no) }
      # puts "Clicking #{new_link}"
      @mech.click(new_link)
    end

    def relative_url(page_no)
      "/s/#{@city_province_country}?search_by_map=true&zoom=#{@zoom}&sw_lat=#{@bounds.sw.lat}&sw_lng=#{@bounds.sw.lng}&ne_lat=#{@bounds.ne.lat}&ne_lng=#{@bounds.ne.lng}&page=#{page_no}"
    end

    def validate_city_province_country!(text)
      fail ArgumentError, "Invalid city '#{text}', should be <city>--<province>--<country>" unless text =~ /^[[:alpha:]]+--[[:alpha:]]+--[[:alpha:]]+$/
    end
  end
end
