require 'json'
require 'facets/kernel/try'
require 'facets/string'
require 'facets/hash'
require_relative 'paged_html_parser'

module PageParsers
  class Listing < PagedHTMLParser
    ATTRIBUTES = %i[id name host_id host_name super_host instant_book business_travel
                max_guests bathrooms bedrooms
                total_beds king_beds queen_beds single_beds double_beds sofa_beds couches cribs
                floor_mattresses toddler_beds air_mattresses bunk_beds water_beds hammocks
                property_type room_type
                extra_people_fee guests_without_extra_fee cleaning_fee security_deposit
                weekly_discount_perc monthly_discount_perc
                cancellation_policy
                amenities
                allows_children allows_infants allows_pets allows_smoking allows_events
                review_count review_score review_score_accuracy review_score_communication review_score_cleanliness
                review_score_location review_score_checkin review_score_value]

    attr_reader *ATTRIBUTES, :api_base_url, :api_key

    def to_s
      def allowance_str(which)
        allowed = public_send("allows_#{which}")

        if allowed
          "allows #{which}"
        else
          "doesn't allow #{which}"
        end
      end

      <<-EOT.squish
        #{id} - #{name}, Host(#{host_id}, #{host_name}, superhost: #{super_host}),
        instant book: #{instant_book}, business travel: #{business_travel}
        #{max_guests} guests,
        #{bathrooms} bathrooms, #{bedrooms} bedrooms,
        #{total_beds} beds (#{king_beds} king, #{queen_beds} queen, #{single_beds} single, #{double_beds} double, #{sofa_beds} sofas, #{couches} couches, #{cribs} cribs, #{floor_mattresses} floor mattresses, #{toddler_beds} toddler beds, #{air_mattresses} air mattresses, #{bunk_beds} bunk beds, #{water_beds} water beds, #{hammocks} hammocks),
        #{property_type}, #{room_type},
        EUR #{extra_people_fee}/night after #{guests_without_extra_fee} guests,
        EUR #{cleaning_fee} cleaning,
        EUR #{security_deposit} security deposit,
        #{weekly_discount_perc}% weekly discount,
        #{monthly_discount_perc}% monthly discount,
        #{cancellation_policy} cancellation,
        amenities: #{amenities},
        #{allowance_str('children')},
        #{allowance_str('infants')},
        #{allowance_str('pets')},
        #{allowance_str('smoking')},
        #{allowance_str('events')},
        review score #{review_score} out of #{review_count} reviews
        (#{review_score_accuracy} accuracy, #{review_score_communication} communication,
         #{review_score_cleanliness} cleanliness, #{review_score_location} location,
         #{review_score_checkin} checkin, #{review_score_value} value)
      EOT
    end

    def initialize(id)
      super(listing_url(id))

      @id = id

      parse_one do |page|
        rollbar_scope(
          last_listing_page: {
            response_code: page.code,
            response_headers: page.response,
            body: page.body,
            listing_json: JSON.dump(listing_json(page))
          })

        @name = listing_name(page)
        @host_id, @host_name, @super_host = listing_host(page)
        @instant_book = listing_instant_book(page)
        @business_travel = listing_business_travel(page)
        @max_guests = listing_max_guests(page)
        @bathrooms = listing_bathrooms(page)
        @bedrooms = listing_bedrooms(page)
        @total_beds = listing_total_beds(page)
        @king_beds, @queen_beds, @single_beds, @double_beds, @sofa_beds, @couches, @cribs, @floor_mattresses,
        @toddler_beds, @air_mattresses, @bunk_beds, @water_beds, @hammocks = listing_beds_detailed(page)
        @property_type = listing_property_type(page)
        @room_type = listing_room_type(page)
        @extra_people_fee, @guests_without_extra_fee = listing_extra_people_charge(page)
        @cleaning_fee = listing_cleaning_fee(page)
        @security_deposit = listing_security_deposit(page)
        @weekly_discount_perc = listing_weekly_discount_perc(page)
        @monthly_discount_perc = listing_monthly_discount_perc(page)
        @cancellation_policy = listing_cancellation_policy(page)
        @amenities = listing_amenities(page)
        @allows_children, @allows_infants, @allows_pets, @allows_smoking, @allows_events = listing_allowances(page)
        @review_count = listing_review_count(page)
        @review_score = listing_review_score(page)
        @review_score_accuracy, @review_score_communication, @review_score_cleanliness,
        @review_score_location, @review_score_checkin, @review_score_value = listing_review_score_detailed(page)

        @api_base_url = listing_api_base_url(page)
        @api_key = listing_api_key(page)
      end
    end

    def attributes_hash
      ATTRIBUTES.map { |attr| [attr, public_send(attr)] }.to_h
    end

    private

    def listing_url(id)
      "https://www.airbnb.com/rooms/#{id}"
    end

    def listing_name(page)
      listing_json(page)['name']
    end

    def listing_host(page)
      host = listing_json(page)['user']
      required = %w[id host_name is_superhost]

      host.require_keys!(required)
      host.slice(*required).values
    end

    def details_div(page, section)
      @details_divs ||= {}
      @details_divs[section] ||= page.css('#details').search('div').text_equals(section).first.next_element
    end

    def extract_html_detail(page, section, detail, node_type: 'span')
      div = details_div(page, section)
      title_span = div.search(node_type).text_equals("#{detail}:").first
      if title_span.nil?
        nil
      else
        title_span.parent.find('strong').text
      end
    end

    def listing_json(page)
      @listing_json ||= begin
        script_text = page.xpath('//script[@data-hypernova-key="listingbundlejs"]').first.text
        JSON.parse(script_text.lchomp('<!--').chomp('-->'))['listing']
      end
    end

    def extract_json_detail(page, section, detail)
      details = listing_json(page)[section]
      details.select { |item| item['label'] == "#{detail}:" }.first.try(:[], 'value')
    end

    def api_config(page)
      @api_config ||= JSON.parse(page.css('meta#_bootstrap-layout-init').attr('content'))['api_config']
    end

    def extract_space_detail(page, detail, default = nil)
      extract_json_detail(page, 'space_interface', detail) || default
    end

    def extract_prices_detail(page, detail, node_type: 'span')
      extract_html_detail(page, 'Prices', detail, node_type: node_type)
    end

    def listing_instant_book(page)
      listing_json(page)['instant_bookable']
    end

    def listing_business_travel(page)
      listing_json(page)['is_business_travel_ready']
    end

    def listing_max_guests(page)
      listing_json(page)['person_capacity']
    end

    def listing_bathrooms(page)
      extract_space_detail(page, 'Bathrooms', 0).to_f
    end

    def listing_bedrooms(page)
      extract_space_detail(page, 'Bedrooms', 0).to_f
    end

    def listing_total_beds(page)
      extract_space_detail(page, 'Beds', 0).to_i
    end

    def listing_property_type(page)
      extract_space_detail(page, 'Property type').downcase
    end

    def listing_room_type(page)
      room_type = extract_space_detail(page, 'Room type')
      case room_type
        when 'Entire home/apt'
          :entire_home
        when 'Private room'
          :private
        when 'Shared room'
          :shared
        else
          fail ArgumentError, "Unknown room type '#{room_type}'"
      end
    end

    def listing_extra_people_charge(page)
      fee = extract_prices_detail(page, 'Extra people')
      case fee
        when /^(.+) \/ night after the first guest$/
          extra_people_fee = $1.to_airbnb_euros
          guests_without_extra_fee = 1
        when /^(.+) \/ night after (\d+) guests$/
          extra_people_fee = $1.to_airbnb_euros
          guests_without_extra_fee = $2.to_i
        when nil, 'No Charge'
          extra_people_fee = BigDecimal.new(0)
          guests_without_extra_fee = 0
        else
          fail ArgumentError, "Unknown 'Extra people' pattern '#{fee}'"
      end

      [extra_people_fee, guests_without_extra_fee]
    end

    def listing_cleaning_fee(page)
      fee = extract_prices_detail(page, 'Cleaning Fee')
      fee.nil? ? BigDecimal(0) : fee.to_airbnb_euros
    end

    def listing_security_deposit(page)
      deposit = extract_prices_detail(page, 'Security Deposit')
      deposit.nil? ? BigDecimal(0) : deposit.to_airbnb_euros
    end

    def listing_weekly_discount_perc(page)
      text = extract_prices_detail(page, 'Weekly discount') || '0%'
      text[/(\d+)%/, 1].to_f
    end

    def listing_monthly_discount_perc(page)
      text = extract_prices_detail(page, 'Monthly discount', node_type: 'a') || '0%'
      text[/(\d+)%/, 1].to_f
    end

    def listing_cancellation_policy(page)
      listing_json(page)['cancellation_policy'].to_sym
    end

    def listing_amenities(page)
      amenities = listing_json(page)['listing_amenities']
      amenities.select { |a| a['is_present'] }.map { |a| a['tag'].underscore.to_sym }
    end

    def listing_beds_detailed(page)
      rooms = listing_json(page)['listing_rooms']

      default_beds = {king: 0, queen: 0, single: 0, double: 0, sofa: 0, couch: 0,
                      crib: 0, floor_mattress: 0, toddler_bed: 0, air_mattress: 0,
                      bunk_bed: 0, water_bed: 0, hammock: 0}
      beds = rooms.reduce(default_beds) do |total, room|
        room['beds'].each do |bed|
          which = case bed['type']
                    when 'king_bed' then :king
                    when 'queen_bed' then :queen
                    when 'single_bed' then :single
                    when 'double_bed' then :double
                    when 'sofa_bed' then :sofa
                    when 'couch' then :couch
                    when 'crib' then :crib
                    when 'floor_mattress' then :floor_mattress
                    when 'toddler_bed' then :toddler_bed
                    when 'air_mattress' then :air_mattress
                    when 'bunk_bed' then :bunk_bed
                    when 'water_bed' then :water_bed
                    when 'hammock' then :hammock
                    else; fail ArgumentError, "Unknown bed type '#{bed['type']}'"
                  end

          total[which] += bed['quantity']
        end

        total
      end

      beds.values
    end

    def listing_allowances(page)
      guest_controls = listing_json(page)['guest_controls']
      required = %w[allows_children allows_infants allows_pets allows_smoking allows_events]

      guest_controls.require_keys!(required)

      guest_controls.slice(*required).values
    end

    def listing_review_count(page)
      review_details = listing_json(page)['review_details_interface']
      review_details['review_count'] || 0
    end

    # @return A number between 0 and 100
    def listing_review_score(page)
      review_details = listing_json(page)['review_details_interface']
      review_details['review_score'] || 0
    end

    # @return A list of numbers between 0 and 100
    def listing_review_score_detailed(page)
      review_summary = listing_json(page)['review_details_interface']['review_summary']

      required = %w[accuracy communication cleanliness location checkin value]

      default_score = required.map { |key| [key, 0] }.to_h
      detailed_score = review_summary.reduce(default_score) do |acc, detail|
        category = detail['category']
        acc[category] = detail['value'] * 10
        acc
      end

      detailed_score.slice(*required).values
    end

    def listing_api_base_url(page)
      api_config(page)['baseUrl']
    end

    def listing_api_key(page)
      api_config(page)['key']
    end
  end
end
