require 'addressable/uri'
require 'mechanize'
require 'nikkou'

module PageParsers
  class InvalidURL < StandardError; end

  class PagedHTMLParser
    include Logging

    def initialize(url)
      @url = url
      @mech = Mechanize.new
      @mech.redirect_ok = false
    end

    def parse_one
      page = retry_on_service_unavailable { @mech.get(url) }
      check_page_exists!(page)

      yield page if block_given?
    end

    def parse_each
      page = retry_on_service_unavailable { @mech.get(url) }
      check_page_exists!(page)

      loop do
        yield page if block_given?

        next_page_link = page.link_with(search: 'div.pagination li.next a')
        break unless next_page_link
        # puts "NEXT: #{next_page_link.uri}"
        page = retry_on_service_unavailable { click_link(next_page_link) }
        check_page_exists!(page)
      end
    end

    private

    def retry_on_service_unavailable(&block)
      sleep_time = 1
      loop do
        begin
          return block.call
        rescue Mechanize::ResponseCodeError => e
          if e.response_code.to_i == 503
            logger.warn "Got 503 error, retrying in #{sleep_time} second(s)"
            sleep sleep_time
            sleep_time *= 2
          else
            raise
          end
        end
      end
    end

    def check_page_exists!(page)
      puts "Got code #{page.code} and location #{page.header['location']}" if page.code.to_i != 200
      fail InvalidURL, "Page '#{page.uri}' does not exist" if page.code.to_i != 200
    end

    def click_link(link)
      link.click
    end

    def add_query_param(url, param_name, param_value)
      uri = Addressable::URI.parse(url)
      params = URI.decode_www_form(uri.query || '') << [param_name, param_value]
      uri.query = URI.encode_www_form(params)
      uri.to_s
    end

    def url
      # TODO: add default currency to config file
      add_query_param(@url, 'currency', 'EUR')
    end
  end
end
