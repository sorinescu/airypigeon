require 'json'
require_relative 'paged_html_parser'

module PageParsers
  class ListingCalendar < PagedHTMLParser
    include Logging

    def initialize(listing, date_range)
      @date_range = date_range
      @listing_id = listing.id

      base_url = listing.api_base_url
      key = listing.api_key
      url = "#{base_url}/v2/calendar_months?key=#{key}&listing_id=#{@listing_id}&locale=en&_format=with_conditions&month=#{start_month}&year=#{start_year}&count=#{month_count}"
      logger.debug  "Calendar URL: #{url}"
      super(url)

      parse_one do |page|
        @calendar_values = calendar_values(JSON.parse(page.body))
      end
    end

    def each(&_block)
      @date_range.each do |date|
        values = @calendar_values[date]
        fail ArgumentError, "No calendar values for date #{date}" if values.nil?

        yield date, values
      end
    end

    private

    def start_year
      @date_range.first.year
    end

    def start_month
      @date_range.first.month
    end

    def month_count
      end_months = @date_range.last.year * 12 + @date_range.last.month
      start_months = start_year * 12 + start_month

      end_months + 1 - start_months
    end

    def calendar_values(data)
      months = data['calendar_months']
      days = months.map do |month|
        days = month['days']
        days.map do |day|
          price = day['price']
          fail ArgumentError, "Unsupported calendar currency #{price['local_currency']}" if price['local_currency'] != 'EUR'

          [Date.parse(day['date']), available: day['available'], price: price['local_price'], price_type: price['type'].to_sym]
        end
      end

      days.flatten(1).to_h
    end
  end
end

