require 'geokit'
require 'json'
require 'mechanize'
require 'facets/kernel'

module PageParsers
  # Should not be used by itself, use CityListings instead
  class ListingsByGeoBounds2
    include Logging

    attr_reader :listing_ids

    def initialize(city_province_country, bounds, zoom)
      validate_city_province_country!(city_province_country)
      fail ArgumentError unless bounds.is_a? Geokit::Bounds

      @mech = Mechanize.new
      @city_province_country = city_province_country
      @bounds = bounds
      @zoom = zoom

      @listing_ids = Set[]
      parse_each do |data|
        @listing_ids.merge data['property_ids']
      end
    end

    private

    def parse_each
      page_no = 1
      results_offset = 0

      loop do
        # logger.debug "Page #{page_no}, offset #{results_offset}"

        page = @mech.get(page_url(page_no), [], nil, 'Accept' => 'application/json')
        data = JSON.parse(page.body)

        break if data['property_ids'].blank?

        yield data if block_given?

        new_offset = data['results_json']['metadata']['pagination']['next_offset']
        break if new_offset == results_offset

        page_no += 1
        results_offset = new_offset
      end
    end

    def page_url(page_no)
      "https://www.airbnb.com/search/search_results?location={@city_province_country}&source=map&airbnb_plus_only=false&search_by_map=true&zoom=#{@zoom}&sw_lat=#{@bounds.sw.lat}&sw_lng=#{@bounds.sw.lng}&ne_lat=#{@bounds.ne.lat}&ne_lng=#{@bounds.ne.lng}&page=#{page_no}"
    end

    def validate_city_province_country!(text)
      fail ArgumentError, "Invalid city '#{text}', should be <city>--<province>--<country>" unless text =~ /^[[:alpha:]]+--[[:alpha:]]+--[[:alpha:]]+$/
    end
  end
end
