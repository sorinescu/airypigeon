require 'geokit'
require 'set'
require_relative 'helpers/latitude_longitude'
require_relative 'listings_by_geo_bounds_2'

module PageParsers
  class CityListings
    include Logging
    include Helpers::LatitudeLongitude

    attr_reader :listing_ids

    def self.listing_url(id)
      "https://www.airbnb.com/rooms/#{id}"
    end

    def initialize(city)
      @geo_city = Geokit::Geocoders::GoogleGeocoder.geocode(city)

      bounds = @geo_city.suggested_bounds
      @listing_ids = listing_ids_within_bounds(bounds).to_a
    end

    private

    def listing_ids_within_bounds(bounds, prefix = '')
      zoom = zoom_for_bounds(bounds, Config.airy_pigeon.map_pixels.width, Config.airy_pigeon.map_pixels.height)

      listings = PageParsers::ListingsByGeoBounds2.new(city_province_country, bounds, zoom)
      ids = Set.new(listings.listing_ids)

      if ids.count >= 290
        logger.debug "#{prefix}Lat: #{bounds.sw.lat} -> #{bounds.ne.lat}, Long: #{bounds.sw.lng} -> #{bounds.ne.lng}, count: #{ids.count}; must recurse"

        split_bounds(bounds).each do |fragment|
          ids.merge(listing_ids_within_bounds(fragment, prefix + '  '))
        end

        logger.debug "#{prefix}Lat: #{bounds.sw.lat} -> #{bounds.ne.lat}, Long: #{bounds.sw.lng} -> #{bounds.ne.lng}, final count: #{ids.count}"
      else
        logger.debug "#{prefix}Lat: #{bounds.sw.lat} -> #{bounds.ne.lat}, Long: #{bounds.sw.lng} -> #{bounds.ne.lng}, count: #{ids.count}"
      end

      ids
    end

    def city_province_country
      "#{@geo_city.city}--#{@geo_city.province}--#{@geo_city.country}"
    end

    def grid_range(start, stop, grid_size)
      step = (stop - start) / grid_size
      range = (start..stop).step(step).to_a

      range << stop unless range.include?(stop)
      range
    end
  end
end
