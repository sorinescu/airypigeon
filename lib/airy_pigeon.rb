require_relative 'initializers'
require_relative 'models/host'
require_relative 'models/listing'
require_relative 'models/listing_date'
require_relative 'page_parsers/city_listings'
require_relative 'page_parsers/listings_by_geo_bounds_2'
require_relative 'page_parsers/listing'
require_relative 'page_parsers/listing_calendar'
require_relative 'processors/listing'
require_relative 'validators/known_listing'
require_relative 'validators/city_listings'

class AiryPigeon
  include Logging

  def validate
    logger.info 'Running validations'
    Validators::KnownListing.validate_all!
    Validators::CityListings.validate!
    logger.info 'Everything looks valid'
  end

  def discover_listings
    city = Config.airy_pigeon.city

    logger.info "Running host discovery for #{city}"

    listing_ids = PageParsers::CityListings.new(city).listing_ids

    DB.transaction do
      discovered_listings_table.delete

      to_insert = listing_ids.map { |id| {id: id} }
      discovered_listings_table.multi_insert to_insert
    end

    logger.info "Finished host discovery for #{city}"
  end

  def update_listings
    discovered_listing_ids.each { |listing_id| update_listing(listing_id) }
  end

  def update_listing(listing_id)
    url = PageParsers::CityListings.listing_url(listing_id)

    logger.debug "Updating listing #{listing_id} from #{url}"

    begin
      begin
        listing = PageParsers::Listing.new(listing_id)
        Processors::Listing.new(listing).save

        update_calendar(listing)
      rescue PageParsers::InvalidURL => e
        Processors::Listing.mark_inactive(listing_id)
        rollbar_info "Listing #{listing_id} from #{url} marked as inactive"
      end
    rescue => e
      logger.error "Exception while processing #{url}: #{e}"
      logger.error e.backtrace

      rollbar_error(e, "Exception while processing listing #{listing_id} at #{url}")
    end
  end

  private

  def discovered_listings_table
    DB[:discovered_listings]
  end

  def discovered_listing_ids
    discovered_listings_table.map { |row| row[:id] }
  end

  def update_calendar(listing)
    date_range = Date.today..(Date.today + 6.months - 1.day)
    calendar = PageParsers::ListingCalendar.new(listing, date_range)

    calendar.each do |date, values|
      logger.debug "Calendar for #{listing.id} on #{date}: #{values}"
      Models::ListingDate.create(values.merge(listing_id: listing.id, date: date))
    end
  end
end
