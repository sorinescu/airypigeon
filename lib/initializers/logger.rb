require 'logger'

module Logging
  class << self
    def logger
      @logger ||= begin
        output = case Config.logging.output.downcase
                   when 'stderr'; STDERR
                   when 'stdout'; STDOUT
                   else; Config.logging.output
                 end

        level = case Config.logging.level.downcase
                  when 'debug'; Logger::DEBUG
                  when 'info'; Logger::INFO
                  when 'warn'; Logger::WARN
                  when 'error'; Logger::ERROR
                  when 'fatal'; Logger::FATAL
                  else fail ArgumentError, "Invalid logger level '#{Config.logging.level}'"
                end

        logger = Logger.new(output)
        logger.level = level
        logger
      end
    end
  end

  # Addition
  def self.included(base)
    class << base
      def logger
        Logging.logger
      end
    end
  end

  def logger
    Logging.logger
  end
end
