require 'rollbar'
require_relative '../../lib/config'

if Config.production?
  Rollbar.configure do |config|
    config.access_token = Config.rollbar.token
  end

  Rollbar.debug('Running AiryPigeon')

  at_exit do
    Rollbar.error($!) if $! # If the program exits due to an exception
    Rollbar.debug('Finished running AiryPigeon')
  end
end

def rollbar_error(*args)
  if Config.production?
    Rollbar.error(*args)
  end
end

def rollbar_info(*args)
  if Config.production?
    Rollbar.info(*args)
  end
end

def rollbar_scope(opts_hash)
  if Config.production?
    Rollbar.scope!(opts_hash)
  end
end