require 'sequel'
require 'pg'
require_relative '../../lib/config'

Sequel.database_timezone = :utc

DB = Sequel.connect(adapter: Config.database.adapter,
                    host: Config.database.host,
                    database: Config.database.database,
                    user: Config.database.user,
                    password: Config.database.password)
DB.extension :pg_array
