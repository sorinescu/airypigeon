require 'json-schema'
require_relative '../page_parsers/city_listings'

module Validators
  class CityListings
    include Logging

    JSON_SCHEMA = {
      type: 'array',
      items: {
        type: 'object',
        required: %w[city count],
        properties: {
          city: { type: 'string' },
          count: { type: 'integer' }
        }
      }
    }.freeze

    def self.validate!
      data = File.read('data/validators/listings.json')
      listings = JSON.parse(data)
      JSON::Validator.validate!(JSON_SCHEMA, listings)

      listings.each do |listing|
        city = listing['city']
        known_count = listing['count']

        logger.info "Validating listings in #{city}"

        listings = PageParsers::CityListings.new(city)
        count = listings.listing_ids.count

        fail ArgumentError,
             "Listing count for '#{city}' should be around #{known_count}, but is #{count}" unless within_tolerance?(count, known_count)

        listings.listing_ids.each do |listing_id|
          fail ArgumentError, "Listing ID '#{listing_id}' is not a positive integer" unless listing_id.is_a?(Integer) && listing_id > 0
        end
      end
    end

    private

    def self.within_tolerance?(actual, wanted)
      tolerance_perc = Config.airy_pigeon.validation.tolerance_perc

      min = wanted * (100.0 - tolerance_perc) / 100.0
      max = wanted * (100.0 + tolerance_perc) / 100.0

      min <= actual && actual <= max
    end
  end
end