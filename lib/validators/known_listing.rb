require 'facets/string'
require 'json-schema'
require_relative '../page_parsers/listing'

module Validators
  class KnownListing
    include Logging

    LISTING_FIELDS_AND_TYPES = {
      id: Integer,
      host_id: Integer,
      host_name: String,
      super_host: Boolean,
      instant_book: Boolean,
      max_guests: Integer,
      bathrooms: Float,
      bedrooms: Float,
      total_beds: Integer,
      king_beds: Integer,
      queen_beds: Integer,
      single_beds: Integer,
      double_beds: Integer,
      sofa_beds: Integer,
      property_type: String,
      room_type: Symbol,
      extra_people_fee: BigDecimal,
      guests_without_extra_fee: Integer,
      cleaning_fee: BigDecimal,
      security_deposit: BigDecimal,
      weekly_discount_perc: Float,
      monthly_discount_perc: Float,
      cancellation_policy: Symbol,
      amenities: [Symbol],
      allows_children: Boolean,
      allows_infants: Boolean,
      allows_pets: Boolean,
      allows_smoking: Boolean,
      allows_events: Boolean,
      review_count: Integer,
      review_score: Integer,
      review_score_accuracy: Integer,
      review_score_communication: Integer,
      review_score_cleanliness: Integer,
      review_score_location: Integer,
      review_score_checkin: Integer,
      review_score_value: Integer
    }.freeze

    def initialize(listing_file)
      data = File.read(listing_file)
      @known_listing = JSON.parse(data)
      JSON::Validator.validate!(self.class.json_schema, @known_listing)

      @listing = PageParsers::Listing.new(@known_listing['id'])
    end

    def validate!
      type_errors = validate_attribute_types
      value_errors = validate_values

      errors = type_errors.merge(value_errors)

      fail ArgumentError, ["Listing #{@listing.id}"].merge(errors) unless errors.empty?
    end

    def self.validate_all!
      Dir.glob('data/validators/known_listings/*.json') do |file_name|
        logger.info "Validating known listing from '#{file_name}'"
        new(file_name).validate!
      end
    end

    private

    def validate_attribute_types
      errors = []

      LISTING_FIELDS_AND_TYPES.each do |field, type|
        value = @listing.public_send(field)

        if type.is_a? Array
          errors << "Field '#{field}' should be Array, but is #{value.class}" unless value.is_a? Array

          subtype = type[0]
          value.each do |item|
            errors << "Field '#{field}' array item '#{item}' should be #{subtype}, but is #{item.class}" unless item.is_a? subtype
          end
        else
          errors << "Field '#{field}' should be #{type}, but is #{value.class}" unless value.is_a? type
        end
      end

      errors
    end

    def validate_values
      errors = []

      LISTING_FIELDS_AND_TYPES.keys.each do |field|
        known_value = known_field_value(field)
        value = @listing.public_send(field)
        errors << "Field '#{field}' should be '#{known_value}', but is '#{value}'" unless same_value?(value, known_value)
      end

      errors
    end

    def same_value?(value_a, value_b)
      value_a = value_a.sort if value_a.respond_to? :sort
      value_b = value_b.sort if value_b.respond_to? :sort

      value_a == value_b
    end

    def known_field_value(field)
      value = @known_listing[field.to_s]
      type = LISTING_FIELDS_AND_TYPES[field]

      # JSON doesn't have a symbol type, so we encode symbols as strings
      if type == Symbol
        value.to_sym
      elsif type.is_a?(Array) && type[0] == Symbol
        value.map(&:to_sym)
      else
        value
      end
    end

    def self.json_schema
      properties = LISTING_FIELDS_AND_TYPES.map do |field, type|
        if type.is_a? Array
          subtype = json_data_type(type[0])
          { field => {type: :array, items: { type: subtype } } }
        else
          { field => { type: json_data_type(type) } }
        end
      end

      {
        type: 'object',
        required: LISTING_FIELDS_AND_TYPES.keys,
        properties: properties
      }
    end

    def self.json_data_type(ruby_type)
      case ruby_type.to_s
        when 'Integer' then :integer
        when 'Float' then :number
        when 'BigDecimal' then :number
        when 'String' then :string
        when 'Symbol' then :string
        when 'Boolean' then :boolean
        else fail ArgumentError, "Unsupported JSON schema type '#{ruby_type}'"
      end
    end
  end
end
