class Array
  def each_with_next(&block)
    res = zip(drop(1))[0..-2]

    if block_given?
      res.each { |x| yield x }
    else
      res
    end
  end
end