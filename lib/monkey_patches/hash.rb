class Hash
  def require_keys!(required_keys)
    fail ArgumentError, 'Expected an array' unless required_keys.is_a? Array

    missing_keys = required_keys - keys
    fail ArgumentError, "Missing keys #{missing_keys.sort} in #{self}" unless missing_keys.empty?
  end
end