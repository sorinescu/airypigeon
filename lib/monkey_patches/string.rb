require 'money'
require 'monetize'

class String
  def to_airbnb_euros
    parsed = match /(\D+)(\d.*)/
    fail ArgumentError, "Invalid money string '#{self}'" if parsed.nil?

    parsed_currency = parsed[1].rstrip
    amount = parsed[2]

    parsed_currency = case parsed_currency
                        when 'lei'; 'RON'
                        else; parsed_currency
                      end

    # Monetize will incorrectly parse RON as ZAR because the starting R is the symbol for ZAR.
    currency = begin
      Money::Currency.new(parsed_currency)
    rescue
      currency_str = Monetize.compute_currency(parsed_currency)
      Money::Currency.wrap(currency_str)
    end

    fail ArgumentError, "Unknown currency '#{parsed_currency}'" if currency.nil?

    # Monetize.parse doesn't fail if it can't understand the currency.
    # Instead, it uses a default currency.
    # So, we have to do this ourselves.
    fractional = Monetize.extract_cents(amount, currency)
    Money.new(fractional, currency).exchange_to(:EUR).amount
  end
end