require 'sequel'

module Models
  class Host < Sequel::Model
    one_to_many :listing_ids
    unrestrict_primary_key

    def self.create_or_update(id, attributes)
      db.transaction do
        model = find(id: id)
        if model
          model.update(attributes)
          model
        else
          create(attributes.merge(id: id))
        end
      end
    end
  end
end
