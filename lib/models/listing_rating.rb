require 'sequel'
require_relative 'listing_association'

module Models
  class ListingRating < Sequel::Model
    include ListingAssociation
  end
end
