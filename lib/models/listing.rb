require 'sequel'
require 'facets/hash'
require_relative 'listing_data'
require_relative 'listing_rating'

module Models
  class Listing < Sequel::Model
    many_to_one :host
    one_to_many :listing_datas
    one_to_many :listing_ratings
    one_to_many :listing_dates

    unrestrict_primary_key

    class << self
      def create_or_update(id, fields)
        db.transaction do
          model = find(id: id)
          if model
            update_listing(model, fields)
          else
            create_listing(id, fields)
          end
        end
      end

      def all_ids
        select(:id).map(&:id)
      end

      private

      def extract_fields(fields, for_class)
        wanted_fields = for_class::REQUIRED_FIELDS
        fields = fields.rekey(id: :listing_id)

        fields.require_keys!(wanted_fields)

        fields.slice(*wanted_fields)
      end

      def listing_data_fields(fields)
        extract_fields(fields, ListingData)
      end

      def listing_rating_fields(fields)
        extract_fields(fields, ListingRating)
      end

      def listing_fields(fields)
        required = [:id, :host_id]
        fields.require_keys!(required)
        fields.slice(*required)
      end

      def create_listing(id, fields)
        fields = fields.merge(id: id)
        db.transaction do
          model = create(listing_fields(fields))
          ListingData.create_if_changed(listing_data_fields(fields))
          ListingRating.create_if_changed(listing_rating_fields(fields))
          model
        end
      end

      def update_listing(model, fields)
        fields = fields.merge(id: model.id)
        db.transaction do
          model.update(listing_fields(fields))
          ListingData.create_if_changed(listing_data_fields(fields))
          ListingRating.create_if_changed(listing_rating_fields(fields))
          model
        end
      end
    end
  end
end
