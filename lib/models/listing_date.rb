require 'sequel'
require_relative 'listing_association'

module Models
  class ListingDate < Sequel::Model
    include ListingAssociation
  end
end
