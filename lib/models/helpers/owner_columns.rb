require 'facets/hash'

module Models
  module Helpers
    module OwnerColumns
      def self.included(base)
        base.class_eval do
          def self.owner_columns
            @owner_columns ||= all_association_reflections.select { |props| props[:type] == :many_to_one }.map { |props| props[:keys] }.flatten
          end
        end
      end

      def owner_columns
        self.class.owner_columns
      end

      def owner_columns_values
        values.slice(*owner_columns)
      end

      def owner_columns_from_fields(fields)
        fields.require_keys!(owner_columns)
        fields.slice(*owner_columns)
      end
    end
  end
end
