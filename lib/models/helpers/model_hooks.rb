module Models
  module Helpers
    module ModelHooks
      def self.included(base)
        base.class_eval do
          def self.before_create_methods
            @before_create_methods ||= []
          end

          def self.before_create(*methods)
            before_create_methods.concat(methods)
          end
        end
      end

      def before_create
        super
        self.class.before_create_methods.each { |name| send(name) }
      end
    end
  end
end
