require 'facets/array'
require 'facets/hash'

module Models
  module Helpers
    module RequiredFields
      def self.included(base)
        unless base.const_defined?(:REQUIRED_FIELDS)
          base.const_set :REQUIRED_FIELDS,
                         base.columns.reject { |c| c.to_s.start_with?('prev_') }.reject_values(:id, :created_at).freeze
        end
      end

      def fields_changed?(fields)
        required = self.class::REQUIRED_FIELDS
        fields.require_keys!(required)

        fields.slice(*required) != values.slice(*required)
      end
    end
  end
end
