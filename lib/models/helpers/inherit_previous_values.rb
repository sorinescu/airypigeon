require_relative 'model_hooks'
require_relative 'owner_columns'

module Models
  module Helpers
    module InheritPreviousValues
      def self.included(base)
        base.class_eval do
          include ModelHooks
          include OwnerColumns

          before_create :inherit_previous_values
        end
      end

      def inherit_previous_values
        prev = self.class.where(owner_columns_values).order(Sequel.desc(:created_at)).first

        columns.select { |c| c.to_s.start_with? 'prev_' }.each do |prev_col|
          col = prev_col.to_s.sub('prev_', '').to_sym
          self[prev_col] = prev[col] unless prev.nil?
        end
      end
    end
  end
end
