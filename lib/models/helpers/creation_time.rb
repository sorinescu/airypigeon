require_relative 'model_hooks'

module Models
  module Helpers
    module CreationTime
      def self.included(base)
        base.class_eval do
          include ModelHooks
          before_create :add_created_at

          def self.latest_for(fields)
            where(fields).order(Sequel.desc(:created_at)).first
          end
        end
      end

      def add_created_at
        self.created_at ||= Time.now
      end
    end
  end
end
