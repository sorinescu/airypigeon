require 'sequel'
require_relative 'listing_association'

module Models
  class ListingData < Sequel::Model
    include ListingAssociation
  end
end
