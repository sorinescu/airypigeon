require_relative 'helpers/creation_time'
require_relative 'helpers/inherit_previous_values'
require_relative 'helpers/required_fields'

module Models
  module ListingAssociation
    def self.included(base)
      base.class_eval do
        include Helpers::CreationTime
        include Helpers::InheritPreviousValues
        include Helpers::RequiredFields

        many_to_one :listing

        def self.create_if_changed(fields)
          listing_id = fields[:listing_id]
          fail ArgumentError, 'Missing listing_id' if listing_id.nil?

          latest = latest_for(listing_id: listing_id)
          if latest.nil? || latest.fields_changed?(fields)
            create(fields)
          else
            latest
          end
        end
      end
    end
  end
end
