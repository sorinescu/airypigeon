require 'rake/testtask'

ENV['AIRY_PIGEON_ENV'] ||= 'development'

namespace :db do
  desc 'Set up database'
  task :setup do
    require_relative 'lib/initializers'

    suffix = '_test' if Config.test?

    sh "psql -U postgres -h #{Config.database.host} -d template1 -f db/database#{suffix}.sql"
    require_relative "db/schema#{suffix}"
  end

  desc 'Run a local Docker DB image'
  task :run do
    sh 'docker run --name airy-pigeon-db -p 5432:5432 -d -v airy-pigeon-dbdata:/var/lib/postgresql/data postgres:9.6'
  end

  desc 'Backup DB to ./airy-pigeon-backup.tar'
  task :backup do
    sh 'docker exec airypigeon_db_1 pg_dump -U airy_pigeon -Fc airy_pigeon > export/dump.db'
  end

  desc 'Restore DB from ./airy-pigeon-backup.tar'
  task :restore do
    sh 'docker exec airypigeon_db_1 pg_restore -U airy_pigeon -n public -c -1 -d airy_pigeon export/dump.db'
  end

  namespace :caravel do
    desc 'Creates the Caravel SQLite settings database in caravel/db/caravel.db'
    task :setup do
      sh 'docker run --rm --env SECRET_KEY="airy_pigeon" --env SQLALCHEMY_DATABASE_URI="sqlite:////home/caravel/db/caravel.db" --volume `pwd`/caravel:/home/caravel/db -it --entrypoint /bin/sh amancevice/caravel -c caravel-init'
    end
  end
end

desc 'Run tests'
task :test do
  ENV['AIRY_PIGEON_ENV'] = 'test'
  Rake::Task['test:runner'].invoke
end

namespace :test do
  Rake::TestTask.new(:runner) do |t|
    t.description = nil
    t.libs << %w[test lib]
    t.pattern = 'test/**/*_test.rb'
    t.warning = false # too many Sequel warnings from Ruby
  end
end

desc "Run an interactive Ruby session a la 'rails console'"
task :pry do
  require 'pry'
  require 'require_all'
  require_relative 'lib/initializers'
  require_all 'lib/models'
  require_all 'lib/page_parsers'
  require_all 'lib/processors'
  require_relative 'lib/airy_pigeon'

  binding.pry
end

desc 'Run host discovery in configured city'
task :discover do
  require_relative 'lib/airy_pigeon'
  AiryPigeon.new.discover_listings
end

desc 'Update host data in configured city'
task :update do
  require_relative 'lib/airy_pigeon'
  AiryPigeon.new.update_listings
end

desc 'Validate AirBnB data against expected structure'
task :validate do
  require_relative 'lib/airy_pigeon'
  AiryPigeon.new.validate
end
